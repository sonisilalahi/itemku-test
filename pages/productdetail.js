import React, { useState } from "react";
import Link from 'next/link'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { BottomSheet } from 'react-spring-bottom-sheet'
import 'react-spring-bottom-sheet/dist/style.css'

function ProductDetail() {
  const [like, setLike] = useState(false)

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2.25,
    }
  };
  const [open, setOpen] = useState(false)
  return (
    <div>
        <div>
            <div className="relative">
                 <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
            </div>
            <div className="w-full absolute top-4 left-0">
               <div className="flex  flex-row justify-between p-1">
                 <div>
                     <div className="bg-slate-600 rounded-md m-1">
                        <Link href="/"><a><img className="w-6" src="/images/left-arrow.png" alt="Sunset in the mountains"/></a></Link>
                     </div>
                </div>
                <div className="flex flex-row justify-between">
                    <div className="bg-slate-600 rounded-md m-1"><img className="w-6" src="/images/share.png" alt="Sunset in the mountains"/></div>
                    <div className="bg-slate-600 rounded-md m-1"><img className="w-6" src="/images/shopping-cart.png" alt="Sunset in the mountains"/></div>
                </div>
               </div>
            </div>
            <div className="p-4">
                <div className="flex flex-row justify-between">
                    <div className="font-bold text-md">366 Diamond</div>
                    <div onClick={() => setLike((prevLike) =>!prevLike)}>
                        { like?
                            <img className="w-5" src="/images/heart-red.png" alt="Sunset in the mountains"/>
                        :
                            <img className="w-5" src="/images/heart.png" alt="Sunset in the mountains"/>

                        }
                            
                   
                    </div>
                </div>
                <div className="text-gray-500 text-xs">Mobile Legend</div>
                <div className="flex flex-row items-center mt-2">
                    <div className="text-orange-500 text-xl font-bold">Rp. 100.000</div><div className="text-gray-500 text-xs ml-2">Per 1 top up</div>
                </div>
                <div className="discountContainer mt-2">
                    <div className="discount">
                        <div className="text-neutral-50 text-xs">50%</div>
                    </div>
                    <div className="pricebefore pl-2">
                        <div className="text-gray-500 text-xs line-through">Rp.200.000</div>
                    </div>
                </div>
            </div>
            <div className="bg-neutral-100 h-4 mt-2"/>
            <div className="p-4">
                <div className="font-bold text-md">Deskripsi Produk</div>
                <div className="text-black text-xs mt-2">Deskripsi Produk Mobile Legend dari toko ulalashop yang paling murah ltifungsi terbuat dari bahan taslan yang tahan air dan tidak mudah ditembus angin.</div>
                <div className="mt-2 flex justify-end">
                    <div onClick={() => setOpen(true)} className="text-sky-600 font-bold text-md">Selengkapnya</div>
                </div>
            </div>
            <div className="bg-neutral-100 h-4 mt-2"/>
            <div className="p-4">
                <div className="flex flex-row justify-between">
                    <div className="font-bold text-md">Pengiriman Tercepat</div>
                    <div className="text-sky-600 font-bold text-md">Selengkapnya</div>
                </div>
                <div className="text-black text-xs mt-2">Produk dari penjual-penjual yang memberi garansi pengiriman 10 menit</div>
                <div className="pl-1">
                    <Carousel 
                    responsive={responsive}
                    arrows ={false}
                    >
                    <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                        <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                        <div className="px-2 py-2">
                        <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                        <div className="text-gray-500 text-xs">
                            Apex Legend PC
                        </div>
                        <div className="ribbonStok mt-2">
                            <div className="text-green-500 text-xs">Stok 999+</div>
                        </div>
                        <div className="mt-2">
                            <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                        </div>
                        <div className="estimate mt-2">
                            <div className="text-green-500 text-xs">10 Menit Kirim</div>
                        </div>
                        <div className="mt-5">
                            <div className="text-gray-500 text-xs">
                                1000 Produk Terjual
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                        <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                        <div className="px-2 py-2">
                        <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                        <div className="text-gray-500 text-xs">
                            Apex Legend PC
                        </div>
                        <div className="ribbonStok mt-2">
                            <div className="text-green-500 text-xs">Stok 999+</div>
                        </div>
                        <div className="mt-2">
                            <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                        </div>
                        <div className="estimate mt-2">
                            <div className="text-green-500 text-xs">10 Menit Kirim</div>
                        </div>
                        <div className="mt-5">
                            <div className="text-gray-500 text-xs">
                                1000 Produk Terjual
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                        <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                        <div className="px-2 py-2">
                        <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                        <div className="text-gray-500 text-xs">
                            Apex Legend PC
                        </div>
                        <div className="ribbonStok mt-2">
                            <div className="text-green-500 text-xs">Stok 999+</div>
                        </div>
                        <div className="mt-2">
                            <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                        </div>
                        <div className="estimate mt-2">
                            <div className="text-green-500 text-xs">10 Menit Kirim</div>
                        </div>
                        <div className="mt-5">
                            <div className="text-gray-500 text-xs">
                                1000 Produk Terjual
                            </div>
                        </div>
                        </div>
                    </div>
                    </Carousel>
                </div>
            </div>
            <BottomSheet open={open}>
                <div className="p-5">
                    <div className="mt-2 flex justify-between items-center">
                        <div className="font-bold text-lg">Deskripsi Produk</div>
                        <img onClick={() => setOpen(false)} className="w-4" src="/images/close.png"/>
                    </div>
                    <div className="mt-2">
                    https://itemku.com/tanya-jawab/artikel/cara-trading-item-pubg-mobile/360007280672
                    .: WELCOME TO VS GAMMING :.
                    UC PUBG Lainnya Silakan Klik --&gt; https://itemku.com/toko/vsgamming?game=94 
                    
                    Cara order, jangan lupa cantumkan Nama/Nick Name Dan User ID
                    Contohnya : VS-Gamming, 12345678

                    Perhatian :
                    Kesalahan menuliskan data (nama/user id) bukan tanggung jawab kami..
                    User ID yang menjadi patokan dan nama hanya sebagai perbandingan. kesalahan memasukan ID adalah fatal
                    Order dengan data yang tidak lengkap tidak akan kami proses
                    Proses apabila lancar 1-5 Menit
                    Jangan lupa konfirmasi "Selesai" apabila UC sudah masuk..
                    Bukan untuk PUBG Korea (Tetap order bukan tanggung jawab kami..)
                    UC dikirim dalam bentuk pecahan hingga jumlahnya sesuai dengan jumlah pada lapak ini...
                    
                    .: Terimakasih Banyak, salam hangat "VS Gamming" :.
                    </div>
                </div>
            </BottomSheet>
        </div>
        <footer class="h-15 p-4 border-t-2">
            <button class=" w-full btn btn-orange">
             Tambah ke Troli
            </button>
        </footer> 
    </div>
  );
}

export default ProductDetail;
