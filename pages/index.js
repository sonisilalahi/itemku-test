import styles from '../styles/home.module.css'
import Link from 'next/link'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";



function Home() {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2.25,
    }
  };
  return (
    <div className='flex flex-col h-screen justify-between'>
      <header  className={styles.header}>
        <img src="images/Logo_itemku.png" className={styles.logo} />
      </header >

      <main>
        <div className="mb-5">
          <div className="px-4 py-2">
              <div className="font-bold text-lg">Termurah di Seluruh Indonesia</div>
          </div>
          <div className="pl-1">
            <Carousel 
            responsive={responsive}
            arrows ={false}
            >
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
            </Carousel>
          </div>
        </div>

        <div className="mb-5">
          <div className="px-4 py-2">
              <div className="font-bold text-lg">Product Mobile Legend Terpopuler</div>
          </div>
          <div className="pl-1">
            <Carousel 
            responsive={responsive}
            arrows ={false}
            >
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>

              <Link href="/productdetail">
                <a>
                  <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                  <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                  <div className="px-2 py-2">
                    <div className="font-bold text-base mb-2">1000 Apex Cointss</div>
                    <div className="text-gray-500 text-xs">
                      Apex Legend PC
                    </div>
                    <div className="ribbonStokLimited mt-2">
                      <div className="text-orange-500 text-xs">Stok 500</div>
                    </div>
                    <div className="discountContainer mt-2">
                        <div className="discount">
                          <div className="text-neutral-50 text-xs">50%</div>
                        </div>
                        <div className="pricebefore pl-2">
                          <div className="text-gray-500 text-xs line-through">Rp.200.000</div>
                        </div>
                    </div>
                    <div className="mt-2">
                      <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                    </div>
                    <div className="estimate mt-2">
                      <div className="text-green-500 text-xs">10 Menit Kirim</div>
                    </div>
                    <div className="mt-5">
                      <div className="text-gray-500 text-xs">
                          1000 Produk Terjual
                      </div>
                    </div>
                  </div>
                </div>
                </a>
              </Link>
              
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
              <div className="max-w-sm rounded-md overflow-hidden shadow-md m-2">
                <img className="w-full" src="/images/ml.png" alt="Sunset in the mountains"/>
                <div className="px-2 py-2">
                  <div className="font-bold text-base mb-2">1000 Apex Coint</div>
                  <div className="text-gray-500 text-xs">
                    Apex Legend PC
                  </div>
                  <div className="ribbonStok mt-2">
                    <div className="text-green-500 text-xs">Stok 999+</div>
                  </div>
                  <div className="mt-2">
                    <div className="text-orange-500 font-bold text-sm">Rp.100.000</div>
                  </div>
                  <div className="estimate mt-2">
                    <div className="text-green-500 text-xs">10 Menit Kirim</div>
                  </div>
                  <div className="mt-5">
                    <div className="text-gray-500 text-xs">
                        1000 Produk Terjual
                    </div>
                  </div>
                </div>
              </div>
            </Carousel>
          </div>
        </div>
      </main>
    
      <footer class="h-15 p-4 border-t-2">
        <div className='text-gray-500 text-xs text-center'>2014 - 2021 PT.Five Jack All Right Reserved.</div>
        <div className='text-gray-500 text-xs text-center'>All other trademarks belong to their respective owners.</div>  
      </footer>  
       
    </div>
    

  );
}

export default Home;
